# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* project assignment for gca at WHS

### How do I get set up? ###

* install java openjdk 11 
* install gradle or just use the gradle wrapper
* install minikube for your system either use the package provided by your package manager or clone the git repo https://github.com/kubernetes/minikube

### Contribution guidelines ###

* only authorized users
* please make sure to write good commit messages not just "some changes"

### Who do I talk to? ###

* Repo owner: Lukas Demming demming99@web.de
* Other community or team contact

### TODO  ###
* implement services
* generate containers one per service
* generate deploy the services into a kubernetes cluster 

