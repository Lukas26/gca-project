package com.webShop.checkout;

import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CheckOutController {
    private Logger logger = LoggerFactory.getLogger(CheckOutController.class);
    @GetMapping("/checkout")
    @Retryable(maxAttempts = 3, backoff = @Backoff(delay = 5))
    public JSONObject getCheckoutInfo(@RequestParam(name="cId") String id){
        final String uri = "http://" + System.getenv("CART_SERVICE_HOST")+ ":"+ System.getenv("CART_SERVICE_PORT")+"/cart?id="+id;
        RestTemplate template = new RestTemplate();
        JSONObject result = template.getForObject(uri, JSONObject.class);
        final String sumUri = "http://" +  System.getenv("CART_SERVICE_HOST") + ":"+ System.getenv("CART_SERVICE_PORT") + "/sum?cId="+id;
        RestTemplate sum = new RestTemplate();
        int sumInt = 0;
        sumInt = sum.getForObject(sumUri, Integer.class);

        return result.appendField("sum", sumInt);
    }
    @Recover
    private void fallback(RuntimeException e) {
        logger.error("something went wrong see the stacktrace below");
        logger.error(String.valueOf(e.getStackTrace()));
    }
    // TODO ask the cart service for the cart with the specific id and then display it

}
