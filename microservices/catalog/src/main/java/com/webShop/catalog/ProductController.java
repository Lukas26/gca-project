package com.webShop.catalog;

import com.webShop.catalog.exceptions.ProductNotFound;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class ProductController {
    private CatalogRepo repo = null;
    public ProductController(CatalogRepo catalog){
        this.repo = catalog;
    }
    // get all available products
    @GetMapping("/products")
    public Collection<Product> getAvailableProducts(){
        return repo.findAll().stream().filter(this::isAvailable).collect(Collectors.toList());
    }
    private boolean isAvailable(Product p){
        return p.isAvailable();
    }
    @GetMapping("/isInCart")
    public void isInCart(@RequestParam(name="id") String id){
        Product p = repo.findById(Long.parseLong(id)).get();
        p.setAvailable(false);
        repo.save(p);
    }
    @GetMapping("/product")
    public Product getProductById(@RequestParam(name="id") String id){

        return repo.findById(Long.parseLong(id)).get();
    }
    @GetMapping("/available")
    public void isAvailableAgain(@RequestParam(name = "id") String id){
        Product p = repo.findById(Long.parseLong(id)).get();
        p.setAvailable(true);
        repo.save(p);

    }
}
