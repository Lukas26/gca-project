package com.webShop.catalog;

import com.webShop.catalog.exceptions.ProductNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class NotFound {
    @ResponseBody @ExceptionHandler(ProductNotFound.class)@ResponseStatus(HttpStatus.NOT_FOUND)
    public String notFoundHandler(ProductNotFound ex){
        return ex.getMessage();
    }

}
