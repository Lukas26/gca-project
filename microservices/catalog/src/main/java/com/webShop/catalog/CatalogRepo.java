package com.webShop.catalog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource
@Repository
public interface CatalogRepo extends JpaRepository<com.webShop.catalog.Product, Long> {
}
