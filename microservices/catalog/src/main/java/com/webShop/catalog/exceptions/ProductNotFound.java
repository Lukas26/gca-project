package com.webShop.catalog.exceptions;

public class ProductNotFound extends RuntimeException{
    public ProductNotFound() {
        super("Products or Product not found");
    }
}
