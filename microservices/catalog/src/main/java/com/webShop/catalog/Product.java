package com.webShop.catalog;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Product {
    @Id @GeneratedValue
    private Long id;
    private @NonNull String name;
    private int price;
    private boolean available = true;
    private String description = "";
    public boolean isAvailable(){
        return this.available;
    }
}
