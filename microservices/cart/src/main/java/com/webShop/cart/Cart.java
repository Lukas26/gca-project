package com.webShop.cart;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


@Entity @Data @NoArgsConstructor
public class Cart {
    @Id @GeneratedValue
    private long id;
    private Vector<String> ids = new Vector<>();
    public void addProduct(Long id){
        ids.add(id.toString());
    }
    public void delete(Long id){
        ids.remove(id.toString());
    }
}
