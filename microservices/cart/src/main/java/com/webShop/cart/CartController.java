package com.webShop.cart;

import net.minidev.json.JSONObject;
import netscape.javascript.JSObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.Optional;
import java.util.Vector;

@RestController
public class CartController {
    private Logger logger = LoggerFactory.getLogger(CartController.class);
    private CartRepo carts = null;
    public CartController(CartRepo repo){this.carts = repo;}

    @GetMapping("/cart")
    @Retryable(maxAttempts = 3, backoff = @Backoff(delay = 10))
    public Cart getCartById(@RequestParam(name="id") String id){
        Optional<Cart> result = carts.findById(Long.parseLong(id));
        logger.info("requested cart with id:"+id);
        if(result.isEmpty()){
            Cart newCart = new Cart();
            newCart.setId(Long.parseLong(id));
            carts.save(newCart);
            logger.info("saved new cart");
            return carts.findById(Long.parseLong(id)).get();
        }
        else return result.get();
    }
    @GetMapping("/sum")
    @Retryable(maxAttempts = 3, backoff = @Backoff(delay = 5))
    public int getCartSum(@RequestParam(name="cId")String id){
        Cart cart = getCartById(id);
        Vector<String> ids = cart.getIds();
        int sum = 0;
        for(String i:ids){
            logger.info("looking for product with id:<"+i+">");
            final String uri = "http://" + System.getenv("CATALOG_SERVICE_HOST") + ":" + System.getenv("CATALOG_SERVICE_PORT")+"/product?id="+i;
            RestTemplate template = new RestTemplate();
            JSONObject result = template.getForObject(uri, JSONObject.class);

            logger.info("found price:"+result.get("price"));
            sum += result.getAsNumber("price").intValue();
        }
        return sum;
    }
    // only for debugging purposes
    @GetMapping("/carts")
    @Retryable(maxAttempts = 3, backoff = @Backoff(delay = 5))
    public Collection<Cart> getAllCarts(){
        return carts.findAll();
    }
    @GetMapping("/add")
    @Retryable(maxAttempts = 3, backoff = @Backoff(delay = 5))
    public Cart addProduct(@RequestParam(name="pId") String productId, @RequestParam(name="cId") String CartId){
        Cart cart = getCartById(CartId);
        logger.info("found cart with id:"+ cart.getId());
        final String uri = "http://" + System.getenv("CATALOG_SERVICE_HOST") +  ":" + System.getenv("CATALOG_SERVICE_PORT")+"/product?id="+productId;
        RestTemplate template = new RestTemplate();

        JSONObject result = template.getForObject(uri, JSONObject.class);
        logger.info("Result:" + result.getAsNumber("id")+ result.getAsString("name"));
        logger.info("is available:"+result.get("available"));

        if(Boolean.parseBoolean(result.getAsString("available"))){
            // add it to the cart
            cart.addProduct(result.getAsNumber("id").longValue());
            // send answer to catalog that its unavailable for now
            final String answer = "http://" +  System.getenv("CATALOG_SERVICE_HOST") + ":" + System.getenv("CATALOG_SERVICE_PORT") + "/isInCart?id="+productId;
            RestTemplate answerTemplate = new RestTemplate();
            answerTemplate.getForObject(answer,Void.class);
            carts.save(cart);
        }
        logger.info(result.toString());
        return cart;
    }
    @GetMapping("/remove")
    @Retryable(maxAttempts = 3, backoff = @Backoff(delay = 5))
    public Cart remove(@RequestParam(name="pId") String productId, @RequestParam(name="cId") String CartId){
        Cart cart = getCartById(CartId);
        logger.info("found cart with id:"+ cart.getId());
        RestTemplate answerTemplate = new RestTemplate();
        final String answer = "http://" + System.getenv("CATALOG_SERVICE_HOST")+ ":"+ System.getenv("CATALOG_SERVICE_PORT")+"/available?id="+productId;
        answerTemplate.getForObject(answer,Void.class);

        cart.delete(Long.parseLong(productId));
        logger.info("deleted product with id:"+productId + "from card with id:"+CartId);
        carts.save(cart);
        return cart;
    }
    @Recover
    public void fallback(RuntimeException e ){
        logger.error("an error occured see stacktrace below");
        logger.error(String.valueOf(e.getStackTrace()));
    }
}
