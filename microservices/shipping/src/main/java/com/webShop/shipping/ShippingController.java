package com.webShop.shipping;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShippingController {
    public ShippingController(){

    }
    @GetMapping("/shipping")
    public static int computeCost(){
        int cartPrice = 100; //TODO get price from cartservice
        if(cartPrice <= 100){
            return 0;
        }
        else return 10;
    }
}
