package com.webShop.shipping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Shipping {
    public static void main(String[] args) {
        SpringApplication.run(Shipping.class, args);
    }
}
